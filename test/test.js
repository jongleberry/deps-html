'use strict'

let fs = require('fs')
let path = require('path')
let assert = require('assert')

let parse5 = require('parse5-utils')
let extract = require('..')

describe('<script>', function () {
  it('with a src', function () {
    let fragment = parse5.parse('<script src="asdf"></script>', true)
    let match = extract(fragment)
    let dep = match[0]
    assert.equal(1, match.length)
    assert.equal('asdf', dep.path)
    assert.equal('script', dep.type)
    assert(!dep.inline)
    assert(dep.node)
    assert(dep.attrs)
    parse5.setAttribute(dep.node, 'src', '1234')
    let html = parse5.stringify(fragment)
    assert.equal('<script src="1234"></script>', html)
  })

  it('without a src', function () {
    let fragment = parse5.parse('<script></script>', true)
    let match = extract(fragment)
    assert.equal(1, match.length)
  })

  it('with inline js', function () {
    let fragment = parse5.parse('<script>lol</script>', true)
    let match = extract(fragment)
    assert.equal(1, match.length)
    let dep = match[0]
    assert(dep.inline)
    assert.equal(dep.type, 'script')
    assert.equal(parse5.textOf(dep.node), 'lol')
    parse5.setText(dep.node, 'a && b')
    let html = parse5.stringify(fragment)
    assert.equal(html, '<script>a && b</script>')
  })
})

describe('<module>', function () {
  it('with a src', function () {
    let fragment = parse5.parse('<module src="asdf"></module>', true)
    let match = extract(fragment)
    let dep = match[0]
    assert.equal(1, match.length)
    assert.equal('asdf', dep.path)
    assert.equal('module', dep.type)
    assert(!dep.inline)
    assert(dep.node)
    assert(dep.attrs)
    parse5.setAttribute(dep.node, 'src', '1234')
    let html = parse5.stringify(fragment)
    assert.equal('<module src="1234"></module>', html)
  })

  it('without a src', function () {
    let fragment = parse5.parse('<module></module>', true)
    let match = extract(fragment)
    assert.equal(1, match.length)
  })
})

describe('<style>', function () {
  it('with inline css', function () {
    let fragment = parse5.parse('<style>lol</style>', true)
    let match = extract(fragment)
    assert.equal(1, match.length)
    let dep = match[0]
    assert.equal(dep.type, 'style')
    assert.equal(parse5.textOf(dep.node), 'lol')
    assert(dep.inline)
  })
})

describe('<link>', function () {
  describe('rel="stylesheet"', function () {
    it('with a href', function () {
      let fragment = parse5.parse('<link rel="stylesheet" href="asdf">', true)
      let match = extract(fragment)
      let dep = match[0]
      assert.equal(1, match.length)
      assert.equal('asdf', dep.path)
      assert.equal('stylesheet', dep.type)
      assert(dep.node)
      assert(dep.attrs)
      parse5.setAttribute(dep.node, 'href', '1234')
      let html = parse5.stringify(fragment)
      assert.equal('<link rel="stylesheet" href="1234">', html)
    })
  })

  describe('rel="import"', function () {
    it('with a href', function () {
      let fragment = parse5.parse('<link rel="import" href="asdf">', true)
      let match = extract(fragment)
      let dep = match[0]
      assert.equal(1, match.length)
      assert.equal('asdf', dep.path)
      assert.equal('import', dep.type)
      assert(dep.node)
      assert(dep.attrs)
      parse5.setAttribute(dep.node, 'href', '1234')
      let html = parse5.stringify(fragment)
      assert.equal('<link rel="import" href="1234">', html)
    })
  })
})

describe('<img>', function () {
  it('should parse the dependency', function () {
    let fragment = parse5.parse('<img src="facebook.png">', true)
    let match = extract(fragment)
    let dep = match[0]
    assert.equal(match.length, 1)
    assert.equal(dep.attrs.src, 'facebook.png')
    assert(dep.node)
    assert(dep.attrs)
    parse5.setAttribute(dep.node, 'src', 'google.svg')
    let html = parse5.stringify(fragment)
    assert.equal('<img src="google.svg">', html)
  })
})

describe('<svg><use xlink:href="">', function () {
  it('should parse the dependency', function () {
    let fragment = parse5.parse(`
      <svg viewBox="0 0 100 100">
         <use xlink:href="defs.svg#icon-1"></use>
      </svg>`)
    let match = extract(fragment)
    let dep = match[0]
    assert.equal(match.length, 1)
    assert.equal(dep.attrs.href, 'defs.svg#icon-1')
    assert.equal(dep.type, 'svg xlink')
    assert(dep.node)
    assert(dep.parentNode)
    assert(dep.attrs)
    parse5.setAttribute(dep.node, 'href', 'google.svg')
    let html = parse5.stringify(fragment)
    assert(/xlink:href="google.svg"/.test(html))
  })
})

describe('background images', function () {
  it('should work with urls without quote', function () {
    let fragment = parse5.parse(`<div style="background-image: url(image.png)"></div>`)
    let matches = extract.backgroundImages(fragment)
    assert.equal(matches.length, 1)
    let match = matches[0]
    assert.equal(match.node.nodeName, 'div')
    match.set('image.jpg')
    let html = parse5.stringify(fragment)
    assert(/\(image.jpg\)/.test(html))
  })

  it('should work with urls with quotes', function () {
    let fragment = parse5.parse(`<div style="background-image: url('image.png')"></div>`)
    let matches = extract.backgroundImages(fragment)
    assert.equal(matches.length, 1)
    let match = matches[0]
    assert.equal(match.node.nodeName, 'div')
    match.set('image.jpg')
    let html = parse5.stringify(fragment)
    assert(/\(image.jpg\)/.test(html))
  })
})

/**
 * We're testing too many implementation details :(
 */

describe('fixtures', function () {
  it('hello-world-element', function () {
    let input = fixture('hello-world-element')
    let fragment = parse5.parse(input, true)
    let match = extract(fragment)
    match.forEach(function (dep) {
      if (dep.path === 'bower_components/platform/platform.js') {
        parse5.setAttribute(dep.node, 'src', 'platform/platform.js')
      } else if (dep.path === 'src/hello-world.html') {
        parse5.setAttribute(dep.node, 'href', 'hello-world.html')
      }
    })
    let html = parse5.stringify(fragment)
    assert.equal(html, fixture('hello-world-element', true))
  })
})

function fixture(name, out) {
  let filename = path.join(__dirname, 'fixtures', name)
  if (out) filename += '.out'
  filename += '.html'
  return fs.readFileSync(filename, 'utf8').trim()
}
