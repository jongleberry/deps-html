'use strict'

const parse5 = require('parse5-utils')

/**
 * @param {Document|DocumentFragment} fragment
 * @returns {Array} dependencies
 */

module.exports = function (fragment) {
  const ret = []
  extract.call(ret, fragment)
  return ret
}

// supported <link rel="">s
const supportedLinkRelation = {
  stylesheet: true,
  import: true,
}

function extract(node) {
  switch (node.nodeName) {
    // <script></script>
    // <script src=""></script>
    case 'script':
    case 'module': {
      const attributes = parse5.attributesOf(node)
      const src = 'src' in attributes ? attributes.src : false
      this.push({
        path: src,
        type: node.nodeName,
        node: node,
        attrs: attributes,
        inline: src === false,
      })
      return
    }
    // <style>
    case 'style':
      this.push({
        path: false,
        type: 'style',
        node: node,
        attrs: parse5.attributesOf(node),
        inline: true,
      })
      return
    // <link rel="stylesheet">
    // <link rel="import">
    case 'link': {
      const attributes = parse5.attributesOf(node)
      if (!('rel' in attributes && 'href' in attributes)) break
      if (!supportedLinkRelation[attributes.rel]) break
      this.push({
        path: attributes.href,
        type: attributes.rel,
        node: node,
        attrs: attributes,
        inline: false,
      })
      return
    }
    case 'img': {
      const attributes = parse5.attributesOf(node)
      const src = 'src' in attributes ? attributes.src : false
      this.push({
        path: src,
        type: 'img',
        node: node,
        attrs: attributes,
      })
      return
    }
    case 'svg': {
      // <svg><use xlink:href></use></svg
      const children = node.childNodes.filter(isNotTextNode)
      if (!children.length) return
      const child = children[0]
      if (child.nodeName !== 'use') return // can it not be the first child?
      const attributes = parse5.attributesOf(child)
      if (!attributes.href) return
      this.push({
        path: attributes.href,
        type: 'svg xlink', // do i need to rename this?
        node: child,
        parentNode: node,
        attrs: attributes,
      })
      return
    }
  }

  const children = node.childNodes
  if (children) children.forEach(extract, this)
}

/**
 * Grab all the background images.
 * Completely separate function as it does not grab this information
 * from tag properties.
 *
 * @param {Node} root
 * @returns {[BackgroundImage]}
 */

const cssurlre = /\burl\s*\(\s*([^\)]+)\s*\)/
module.exports.backgroundImages = function extractBackgroundImages(root) {
  const images = []
  for (const node of parse5.flatten(root)) {
    const attributes = parse5.attributesOf(node) // TODO: use .getAttribute()
    if (!attributes.style) continue
    const m = cssurlre.exec(attributes.style)
    if (!m) continue
    images.push(new BackgroundImage(node, m))
  }
  return images
}

function BackgroundImage(node, m) {
  this.node = node
  this.m = m
  this.uri = m[1].replace(/^['"]/, '').replace(/['"]$/, '')
}

BackgroundImage.prototype.set = function (url) {
  // get the attribute again in case it got updated
  const attributes = parse5.attributesOf(this.node) // TODO: use .getAttribute()
  parse5.setAttribute(this.node, 'style', attributes.style.replace(this.m[0], `url(${url})`))
}

function isNotTextNode(node) {
  return node.nodeName !== '#text'
}
