
# deps-html

[![NPM version][npm-image]][npm-url]
[![build status][travis-image]][travis-url]
[![Test coverage][coveralls-image]][coveralls-url]

Parse the external dependencies of an HTML string as well as optionally replace them.
Useful for build systems and package managers that need to rewrite URLs.

```js
var deps = require('deps-html');

var string = '<link rel="stylesheet" href="normalize.css">';
// build the node tree
var ast = deps.parse(string);
// parse all the dependencies
var matches = deps.extract(ast);
// rewrite the `href`s
matches.forEach(function (dep) {
  if (dep.type === 'stylesheet' && dep.path === 'normalize.css') {
    dep.attr.value = './normalize.css';
  }
})
// return the new HTML
var html = deps.stringify(ast)
// html === '<link rel="stylesheet" href="./normalize.css">'
```

## API

### var ast = deps.parse(string)

Returns a [parse5](https://github.com/inikulin/parse5) tree.

### var matches = deps.extract(ast)

Parses all the relevant dependencies from the document.
Each match has the following properties:

- `path` - the `href` or `src` attribute value
- `type` - the type of dependency. The current types are:
    - `<script>`
    - `<module>`
    - `<style>`
    - `<link>`
      - `ref="import"`
      - `ref="stylesheet"`
    - `<img>`
    - `svg xlink`
- `node`- the `parse5` node for this dependency
- `attr` - the raw `parse5` attribute for the node containing `href` or `src`.
  Set `attr.value=` to change the value of the URL.

You are expected to use a module such as [parse5-utils](https://github.com/webdeps/parse5-utils) to manipulate the HTML elements.

### var html = deps.stringify(ast)

Stringify the AST, keeping the relevant styles intact.
It doesn't keep everything intact, which is annoying,
but most of the styles remain intact.

[npm-image]: https://img.shields.io/npm/v/deps-html.svg?style=flat
[npm-url]: https://npmjs.org/package/deps-html
[travis-image]: https://img.shields.io/travis/webdeps/deps-html.svg?style=flat
[travis-url]: https://travis-ci.org/webdeps/deps-html
[coveralls-image]: https://img.shields.io/coveralls/webdeps/deps-html.svg?style=flat
[coveralls-url]: https://coveralls.io/r/webdeps/deps-html?branch=master
